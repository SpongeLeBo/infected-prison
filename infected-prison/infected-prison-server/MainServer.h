#pragma once

#include <SFML/Network.hpp>

class MainServer
{
public:
	MainServer();
	~MainServer();

	void start();
private:
	sf::UdpSocket _socket;
	void sendMap(sf::IpAddress destination);
	std::vector<std::vector<int>> _level;
};

