#include "stdafx.h"
#include "MainServer.h"

#include <string>
#include <iostream>

sf::Packet& operator <<(sf::Packet& packet, const std::vector<std::vector<int>>& myVec)
{
	for (size_t i = 0; i < myVec.size(); i++)
	{
		for (size_t j = 0; j < myVec[0].size(); j++)
		{
			packet << myVec[i][j];
		}
	}
	return packet;
}

MainServer::MainServer()
{
	// liage de la socket au port 
	if (_socket.bind(9999) != sf::Socket::Done)
	{
		throw std::string("Impossible de li� la socket au port 9999");
	}

	std::cout << "Lancement du serveur en attente de nouvelles connexions..." << std::endl;
	// cr�ation intiale de la map
	for (size_t i = 0; i < 8; i++)
	{
		std::vector<int> temp;
		for (size_t j = 0; j < 16; j++)
		{
			if (j == 0 || j == 15 || i == 0 || i == 7)
				temp.push_back(40);
			else
				temp.push_back(0);
		}
		_level.push_back(temp);
	}
}


MainServer::~MainServer()
{
	_socket.unbind();
}

// Lancement du serveur
void MainServer::start() {
	sf::IpAddress sender;
	unsigned short port;
	sf::Packet packet;
	if (_socket.receive(packet, sender, port) != sf::Socket::Done)
	{
		throw std::string("Erreur lors de la r�c�ption du paquet");
	}
	std::string test;
	packet >> test;
	std::cout << "Recu \"" << test << "\" de " << sender << std::endl;
	sendMap(sender);
}

// Envoie la map � un joueur
void MainServer::sendMap(sf::IpAddress destination) {
	std::cout << "Envoie de la map a : " << destination << std::endl;
	unsigned short port = 10000;
	sf::Packet packet;
	packet << _level;
	if (_socket.send(packet, destination, port) != sf::Socket::Done)
	{
		throw std::string("Erreur lors de l'envoie du paquet de la map");
	}
}