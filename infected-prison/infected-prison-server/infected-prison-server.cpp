// infected-prison-server.cpp : définit le point d'entrée pour l'application console.
//

#include "stdafx.h"

#include "MainServer.h"

#include <iostream>

int main()
{
	MainServer server;
	server.start();

	// pour éviter que la console ne se ferme
	int end;
	std::cin >> end;
    return 0;
}

