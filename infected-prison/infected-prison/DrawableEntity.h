#pragma once
#include <SFML/Graphics.hpp>

class DrawableEntity : public sf::Drawable, public sf::Transformable
{
public:
	DrawableEntity();
	~DrawableEntity();

	// Renvoit le centre de l'entit� et pas le coin en haut � gauche
	virtual sf::Vector2f getPosition() const;

	/* Orientation de l'entite vers la position */
	void orientate(const sf::Vector2f& position);

	std::vector<sf::Vector2f> getCorners();

protected:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	sf::Sprite _sprite;
	sf::Texture _texture;
};

