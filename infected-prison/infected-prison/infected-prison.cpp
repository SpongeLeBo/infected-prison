// infected-prison.cpp : définit le point d'entrée pour l'application console.
//

#include "stdafx.h"

#include "Application.h"
#include <iostream>

int main()
{
	Application app;
	app.start();
	return 0;
}